﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardGame_GameController : MonoBehaviour {

    [SerializeField]
    private CurrentCards_Controller cardControllerScript;

    private float mgX = ((float)476 / (float)2560);
    private float mgY = ((float)298 / (float)1600);

    [SerializeField]
    private Canvas canvas;

    public Vector2 playerpos;
    [SerializeField]
    private Image player;

    public Vector2 targetpos;

    public Vector2 enemypos = new Vector2(4, 4);
    [SerializeField]
    private Image enemy;

    public Vector2 selectedGridPos;

    public Sprite normalGrid;
    public Sprite redGrid;

    [SerializeField]
    private Image imgFG;

    private double moveCooldown = 0.0;

    private bool Redcolorjoystick = false;

    // Use this for initialization
    void Start () {

        player.transform.localPosition = new Vector3(((float)playerpos.x / (float)8) * canvas.pixelRect.width + (1 / 8) * canvas.pixelRect.width - 1 * canvas.pixelRect.width, ((float)playerpos.y / (float)5) * canvas.pixelRect.height + (1 / 5) * canvas.pixelRect.height - 1 * canvas.pixelRect.height, 0);
        enemy.transform.localPosition = new Vector3(((float)enemypos.x / (float)8) * canvas.pixelRect.width + (1 / 8) * canvas.pixelRect.width - 1 * canvas.pixelRect.width, ((float)enemypos.y / (float)5) * canvas.pixelRect.height + (1 / 5) * canvas.pixelRect.height - 1 * canvas.pixelRect.height, 0);
        targetpos = playerpos;
    }

    // Update is called once per frame
    void Update () {

        if (moveCooldown > 0.0)
        {
            if (Redcolorjoystick)
            {
                if (moveCooldown > 0.6)
                {
                    var temp = imgFG.color;
                    temp.g = 0;
                    temp.b = 0;
                    imgFG.color = temp;
                }
                else if (moveCooldown > 0.4)
                {
                    var temp = imgFG.color;
                    temp.g = 255;
                    temp.b = 255;
                    imgFG.color = temp;
                }
                else if (moveCooldown > 0.2)
                {
                    var temp = imgFG.color;
                    temp.g = 0;
                    temp.b = 0;
                    imgFG.color = temp;
                }
                else if (moveCooldown > 0.0)
                {
                    var temp = imgFG.color;
                    temp.g = 255;
                    temp.b = 255;
                    imgFG.color = temp;
                }
            }
            moveCooldown -= Time.deltaTime;
        }

        if (moveCooldown <= 0)
        {
            if(targetpos != playerpos)
            playerpos = targetpos;

            moveCooldown = 0;
            Redcolorjoystick = false;
        }

        float moveHorizontal = imgFG.rectTransform.anchoredPosition.x * 0.05f;  //Input.GetAxis ("Horizontal");
        float moveVertical = imgFG.rectTransform.anchoredPosition.y * 0.05f; //Input.GetAxis ("Vertical");

        if (moveCooldown <= 0.0)
        {
            if ((moveHorizontal == 0 && moveVertical == 0) || Mathf.Sqrt(Mathf.Pow(moveHorizontal, 2) + Mathf.Pow(moveVertical, 2)) < 1.75f)
            {
                //N.A
            }
            else if (Mathf.Abs(moveHorizontal) > Mathf.Abs(moveVertical))
            {
                if (moveHorizontal >= 0)
                {
                    //Debug.Log("Right");
                    if (NotWalledTile((int)playerpos.x + 1, (int)playerpos.y))
                    {
                        //playerpos.x += 1;
                        targetpos.x += 1;
                        moveCooldown = 0.8;
                    }
                    else
                    {
                        Redcolorjoystick = true;
                        moveCooldown = 0.8;
                    }
                }
                else
                {
                    //Debug.Log("Left");
                    if (NotWalledTile((int)playerpos.x - 1, (int)playerpos.y))
                    {
                        //playerpos.x -= 1;
                        targetpos.x -= 1;
                        moveCooldown = 0.8;
                    }
                    else
                    {
                        Redcolorjoystick = true;
                        moveCooldown = 0.8;
                    }
                }
            }
            else
            {
                if (moveVertical >= 0)
                {
                    //Debug.Log("Up");
                    if (NotWalledTile((int)playerpos.x, (int)playerpos.y + 1))
                    {
                        //playerpos.y += 1;
                        targetpos.y += 1;
                        moveCooldown = 0.8;
                    }
                    else
                    {
                        Redcolorjoystick = true;
                        moveCooldown = 0.8;
                    }
                }
                else
                {
                    //Debug.Log("Down");
                    if (NotWalledTile((int)playerpos.x, (int)playerpos.y - 1))
                    {
                        //playerpos.y -= 1;
                        targetpos.y -= 1;
                        moveCooldown = 0.8;
                    }
                    else
                    {
                        Redcolorjoystick = true;
                        moveCooldown = 0.8;
                    }
                }
            }
        }

        if (cardControllerScript.CardsLeft <= 0 && cardControllerScript.AllEmpty())
        {
            cardControllerScript.CardsLeft = 10;
            cardControllerScript.ClearDiscardedCard();
        }

        if (cardControllerScript.IsDragging()) //16x10 map in this case
        {
            int xGrid = (int)(16 * (Input.mousePosition.x / (canvas.pixelRect.width )));
            int yGrid = (int)(10 * (Input.mousePosition.y / (canvas.pixelRect.height )));

            string chosencardname = cardControllerScript.GetSelectedCardName();
            bool cardCanBeDropped = false;
            if (chosencardname == "KING CRIMSON") //range of 3 tiles from player
            {
                bool CanBeValid = false;
                for (int y = (int)playerpos.y - 3; y <= (int)playerpos.y + 3; ++y)
                {
                    for (int x = (int)playerpos.x - 3 + Mathf.Abs((int)playerpos.y - y); x <= (int)playerpos.x + 3 - Mathf.Abs((int)playerpos.y - y); ++x)
                    {
                        if (CanBeValid) break;

                        if ((xGrid == x && yGrid == y) && !(xGrid == (int)playerpos.x && yGrid == (int)playerpos.y)) CanBeValid = true;
                    }
                    if (CanBeValid) break;
                }

                for (int y = (int)playerpos.y - 3; y <= (int)playerpos.y + 3; ++y)
                {
                    for (int x = (int)playerpos.x - 3 + Mathf.Abs((int)playerpos.y - y); x <= (int)playerpos.x + 3 - Mathf.Abs((int)playerpos.y - y); ++x)
                    {
                        if (x == (int)playerpos.x && y == (int)playerpos.y)
                        {
                            continue;
                        }

                        //Debug.Log(x + " " + y);
                        if (NotWalledTile(x, y))
                        {
                            //Show red tile at where the player has highlighted
                            if (CanBeValid && (xGrid == x && yGrid == y) && !(xGrid == (int)playerpos.x && yGrid == (int)playerpos.y))
                            {
                                cardCanBeDropped = true;
                                selectedGridPos = new Vector2(x, y);

                                var go = new GameObject("grid");
                                go.transform.SetParent(canvas.transform);
                                go.transform.SetAsFirstSibling();
                                Image tempGridImg = go.AddComponent<Image>();
                                tempGridImg.sprite = redGrid;
                                tempGridImg.raycastTarget = false;
                                go.transform.localScale = new Vector3(1, 1, 1);
                                RectTransform rt = go.GetComponent(typeof(RectTransform)) as RectTransform;
                                rt.sizeDelta = new Vector2(50, 50.1139f);

                                go.transform.localPosition = new Vector3(((float)x / (float)8) * canvas.pixelRect.width * mgX + (1 / 8) * canvas.pixelRect.width * mgX - 1 * canvas.pixelRect.width * mgX, ((float)y / (float)5) * canvas.pixelRect.height * mgY + (1 / 5) * canvas.pixelRect.height * mgY - 1 * canvas.pixelRect.height * mgY, 0);
                                //tempGridImg.SetNativeSize();
                                Destroy(go, Time.deltaTime * 2.0f);
                            }
                            else if ((yGrid == y) && ((x == xGrid + 1 && xGrid > playerpos.x) || (x == xGrid - 1 && xGrid < playerpos.x))) //can also show red tile if its an affected area
                            {
                                if (CanBeValid)
                                {
                                    var go = new GameObject("grid");
                                    go.transform.SetParent(canvas.transform);
                                    go.transform.SetAsFirstSibling();
                                    Image tempGridImg = go.AddComponent<Image>();
                                    tempGridImg.sprite = redGrid;
                                    tempGridImg.raycastTarget = false;
                                    go.transform.localScale = new Vector3(1, 1, 1);
                                    RectTransform rt = go.GetComponent(typeof(RectTransform)) as RectTransform;
                                    rt.sizeDelta = new Vector2(50, 50.1139f);
                                    go.transform.localPosition = new Vector3(((float)x / (float)8) * canvas.pixelRect.width * mgX + (1 / 8) * canvas.pixelRect.width * mgX - 1 * canvas.pixelRect.width * mgX, ((float)y / (float)5) * canvas.pixelRect.height * mgY + (1 / 5) * canvas.pixelRect.height * mgY - 1 * canvas.pixelRect.height * mgY, 0);
                                    Destroy(go, Time.deltaTime * 2.0f);
                                }
                            }
                            else if (xGrid == (int)playerpos.x && xGrid == x && ((y == yGrid + 1 && yGrid > playerpos.y) || (y == yGrid - 1 && yGrid < playerpos.y))) //can also show red tile if its an affected area
                            {
                                if (CanBeValid)
                                {
                                    var go = new GameObject("grid");
                                    go.transform.SetParent(canvas.transform);
                                    go.transform.SetAsFirstSibling();
                                    Image tempGridImg = go.AddComponent<Image>();
                                    tempGridImg.sprite = redGrid;
                                    tempGridImg.raycastTarget = false;
                                    go.transform.localScale = new Vector3(1, 1, 1);
                                    RectTransform rt = go.GetComponent(typeof(RectTransform)) as RectTransform;
                                    rt.sizeDelta = new Vector2(50, 50.1139f);
                                    go.transform.localPosition = new Vector3(((float)x / (float)8) * canvas.pixelRect.width * mgX + (1 / 8) * canvas.pixelRect.width * mgX - 1 * canvas.pixelRect.width * mgX, ((float)y / (float)5) * canvas.pixelRect.height * mgY + (1 / 5) * canvas.pixelRect.height * mgY - 1 * canvas.pixelRect.height * mgY, 0);
                                    Destroy(go, Time.deltaTime * 2.0f);
                                }
                            }
                            else //show yellow tile otherwise
                            {
                                var go = new GameObject("grid");
                                go.transform.SetParent(canvas.transform);
                                go.transform.SetAsFirstSibling();
                                Image tempGridImg = go.AddComponent<Image>();
                                tempGridImg.sprite = normalGrid;
                                tempGridImg.raycastTarget = false;
                                go.transform.localScale = new Vector3(1, 1, 1);
                                RectTransform rt = go.GetComponent(typeof(RectTransform)) as RectTransform;
                                rt.sizeDelta = new Vector2(50, 50.1139f);
                                go.transform.localPosition = new Vector3(((float)x / (float)8) * canvas.pixelRect.width * mgX + (1 / 8) * canvas.pixelRect.width * mgX - 1 * canvas.pixelRect.width * mgX, ((float)y / (float)5) * canvas.pixelRect.height * mgY + (1 / 5) * canvas.pixelRect.height * mgY - 1 * canvas.pixelRect.height * mgY, 0);
                                Destroy(go, Time.deltaTime * 2.0f);
                            }

                        }
                    }
                }
            }
            else if (chosencardname == "EUPHORIA") //on player only
            {
                if (xGrid == (int)playerpos.x && yGrid == (int)playerpos.y)
                {
                    cardCanBeDropped = true;
                    selectedGridPos = new Vector2(xGrid, yGrid);

                    var go = new GameObject("grid");
                    go.transform.SetParent(canvas.transform);
                    go.transform.SetAsFirstSibling();
                    Image tempGridImg = go.AddComponent<Image>();
                    tempGridImg.sprite = redGrid;
                    tempGridImg.raycastTarget = false;
                    go.transform.localScale = new Vector3(1, 1, 1);
                    RectTransform rt = go.GetComponent(typeof(RectTransform)) as RectTransform;
                    rt.sizeDelta = new Vector2(50, 50.1139f);
                    go.transform.localPosition = new Vector3(((float)xGrid / (float)8) * canvas.pixelRect.width * mgX + (1 / 8) * canvas.pixelRect.width * mgX - 1 * canvas.pixelRect.width * mgX, ((float)yGrid / (float)5) * canvas.pixelRect.height * mgY + (1 / 5) * canvas.pixelRect.height * mgY - 1 * canvas.pixelRect.height * mgY, 0);
                    Destroy(go, Time.deltaTime * 2.0f);
                }
                else
                {
                    var go = new GameObject("grid");
                    go.transform.SetParent(canvas.transform);
                    go.transform.SetAsFirstSibling();
                    Image tempGridImg = go.AddComponent<Image>();
                    tempGridImg.sprite = normalGrid;
                    tempGridImg.raycastTarget = false;
                    go.transform.localScale = new Vector3(1, 1, 1);
                    RectTransform rt = go.GetComponent(typeof(RectTransform)) as RectTransform;
                    rt.sizeDelta = new Vector2(50, 50.1139f);
                    go.transform.localPosition = new Vector3(((float)playerpos.x / (float)8) * canvas.pixelRect.width * mgX + (1 / 8) * canvas.pixelRect.width * mgX - 1 * canvas.pixelRect.width * mgX, ((float)playerpos.y / (float)5) * canvas.pixelRect.height * mgY + (1 / 5) * canvas.pixelRect.height * mgY - 1 * canvas.pixelRect.height * mgY, 0);
                    Destroy(go, Time.deltaTime * 2.0f);
                }
            }
            else if (chosencardname == "CUT") // range of 2 tiles from player, must be a faceable direction (up down left right only)
            {
                bool CanBeValid = false;
                for (int y = (int)playerpos.y - 3; y <= (int)playerpos.y + 3; ++y)
                {
                    for (int x = (int)playerpos.x - 3 + Mathf.Abs((int)playerpos.y - y); x <= (int)playerpos.x + 3 - Mathf.Abs((int)playerpos.y - y); ++x)
                    {
                        if (y != (int)playerpos.y && x != (int)playerpos.x)
                        {
                            continue;
                        }

                        if (CanBeValid) break;

                        if ((xGrid == x && yGrid == y) && !(xGrid == (int)playerpos.x && yGrid == (int)playerpos.y)) CanBeValid = true;
                    }
                    if (CanBeValid) break;
                }

                for (int y = (int)playerpos.y - 3; y <= (int)playerpos.y + 3; ++y)
                {
                    for (int x = (int)playerpos.x - 3 + Mathf.Abs((int)playerpos.y - y); x <= (int)playerpos.x + 3 - Mathf.Abs((int)playerpos.y - y); ++x)
                    {
                        if (y != (int)playerpos.y && x != (int)playerpos.x)
                        {
                            continue;
                        }

                        if (x == (int)playerpos.x && y == (int)playerpos.y)
                        {
                            continue;
                        }

                        //Debug.Log(x + " " + y);
                        if (NotWalledTile(x, y))
                        {
                            //Show red tile at where the player has highlighted
                            if (CanBeValid && (xGrid == x && yGrid == y) && !(xGrid == (int)playerpos.x && yGrid == (int)playerpos.y))
                            {
                                cardCanBeDropped = true;
                                selectedGridPos = new Vector2(x, y);

                                var go = new GameObject("grid");
                                go.transform.SetParent(canvas.transform);
                                go.transform.SetAsFirstSibling();
                                Image tempGridImg = go.AddComponent<Image>();
                                tempGridImg.sprite = redGrid;
                                tempGridImg.raycastTarget = false;
                                go.transform.localScale = new Vector3(1, 1, 1);
                                RectTransform rt = go.GetComponent(typeof(RectTransform)) as RectTransform;
                                rt.sizeDelta = new Vector2(50, 50.1139f);

                                go.transform.localPosition = new Vector3(((float)x / (float)8) * canvas.pixelRect.width * mgX + (1 / 8) * canvas.pixelRect.width * mgX - 1 * canvas.pixelRect.width * mgX, ((float)y / (float)5) * canvas.pixelRect.height * mgY + (1 / 5) * canvas.pixelRect.height * mgY - 1 * canvas.pixelRect.height * mgY, 0);                              //tempGridImg.SetNativeSize();
                                Destroy(go, Time.deltaTime * 2.0f);
                            }
                            else //show yellow tile otherwise
                            {
                                var go = new GameObject("grid");
                                go.transform.SetParent(canvas.transform);
                                go.transform.SetAsFirstSibling();
                                Image tempGridImg = go.AddComponent<Image>();
                                tempGridImg.sprite = normalGrid;
                                tempGridImg.raycastTarget = false;
                                go.transform.localScale = new Vector3(1, 1, 1);
                                RectTransform rt = go.GetComponent(typeof(RectTransform)) as RectTransform;
                                rt.sizeDelta = new Vector2(50, 50.1139f);
                                go.transform.localPosition = new Vector3(((float)x / (float)8) * canvas.pixelRect.width * mgX + (1 / 8) * canvas.pixelRect.width * mgX - 1 * canvas.pixelRect.width * mgX, ((float)y / (float)5) * canvas.pixelRect.height * mgY + (1 / 5) * canvas.pixelRect.height * mgY - 1 * canvas.pixelRect.height * mgY, 0);
                                Destroy(go, Time.deltaTime * 2.0f);
                            }

                        }
                    }
                }
            }

            if (!cardCanBeDropped)
            {
                selectedGridPos = new Vector2(-100, -100);
            }

            cardControllerScript.SetValidCardUse(cardCanBeDropped);


        }

        Vector2 difference = new Vector2(0, 0);

        if (targetpos != playerpos)
        difference = new Vector2((float)(targetpos.x - playerpos.x) * ((0.8f - (float)moveCooldown) / 0.8f), (float)(targetpos.y - playerpos.y) * ((0.8f - (float)moveCooldown)/0.8f));

        player.transform.localPosition = new Vector3(((float)playerpos.x / (float)8) * canvas.pixelRect.width * mgX + (1 / 8) * canvas.pixelRect.width * mgX - 1 * canvas.pixelRect.width * mgX + difference.x * ((float)1 / (float)8) * canvas.pixelRect.width * mgY, ((float)playerpos.y / (float)5) * canvas.pixelRect.height * mgY  + (1 / 5) * canvas.pixelRect.height * mgY - 1 * canvas.pixelRect.height * mgY + ((float)1 / (float)5) * canvas.pixelRect.height * difference.y * mgY, 0);
        enemy.transform.localPosition = new Vector3(((float)enemypos.x / (float)8) * canvas.pixelRect.width * mgX + (1 / 8) * canvas.pixelRect.width * mgX - 1 * canvas.pixelRect.width * mgX, ((float)enemypos.y / (float)5) * canvas.pixelRect.height * mgY + (1 / 5) * canvas.pixelRect.height * mgY - 1 * canvas.pixelRect.height * mgY, 0);


    }

    public bool NotWalledTile(int _x, int _y)
    {
        if (_x < 0 || _x > 15) return false;

        if (_y < 0 || _y > 9) return false;

        if (_x == 0 && _y == 0) return false;

        if ((_x >= 0 && _x <= 1) && (_y >= 6 && _y <= 9)) return false;

        if ((_x >= 14 && _x <= 15)) return false;

        if ((_x >= 4 && _x <= 13) && (_y >= 7 && _y <= 9)) return false;

        return true;
    }

}
