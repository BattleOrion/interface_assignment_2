﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrentCards_Controller : MonoBehaviour {

    public int CardsLeft;

    [SerializeField]
    private CardSlot_Controller cardslot1;
    [SerializeField]
    private CardSlot_Controller cardslot2;
    [SerializeField]
    private CardSlot_Controller cardslot3;
    [SerializeField]
    private CardSlot_Controller cardslot4;

    [SerializeField]
    private Statistics_Script statistics;

    [SerializeField]
    private Text cardslefttext;

    public Image SelectedCard;
    public Sprite standbycard_none, standbycard_KingCrimson, standbycard_Euphoria, standbycard_Cut;
    bool IsRotating = false;
    float rotateMag = 0;

    public Image discardedCard;

    // Use this for initialization
    void Start () {
        Random.InitState(999);
        discardedCard.sprite = null;
        //InvokeRepeating("DoRotate", 0.0f, 3.5f);
    }

    public void SetDiscardedCard(Sprite _discarded)
    {
        discardedCard.sprite = _discarded;
    }

    public void ClearDiscardedCard()
    {
        discardedCard.sprite = null;
        var temp = discardedCard.color;
        temp.a = 0.0f;
        discardedCard.color = temp;
    }

    void DoRotate()
    {
        if (!IsRotating && SelectedCard.sprite != standbycard_none)
        {
            IsRotating = true;
            rotateMag = 5;
            SelectedCard.transform.eulerAngles = new Vector3(0, 0, 0);
        }
    }
	
    public bool AllEmpty()
    {
        return !cardslot1.Occupied() && !cardslot2.Occupied() && !cardslot3.Occupied() && !cardslot4.Occupied();
    }

    public void DeselectAll()
    {
        cardslot1.Deselect();
        cardslot2.Deselect();
        cardslot3.Deselect();
        cardslot4.Deselect();
    }

	// Update is called once per frame
	void Update () {

        //var temp = SelectedCard.color;
        //temp.a = 0.2f;
        //SelectedCard.color = temp;

        //SelectedCard.sprite = standbycard_none;

        if (cardslot1.CheckIfSelected())
        {
            if (cardslot1.GetCardName() == "NONE") //Card was removed
            {
                rotateMag = 0;
                IsRotating = false;
                SelectedCard.transform.eulerAngles = new Vector3(0, 0, 0);

                var temp = SelectedCard.color;
                temp.a = 0.2f;
                SelectedCard.color = temp;

                SelectedCard.sprite = standbycard_none;

                string tempDiscardedCardname = cardslot1.GetDiscardedCard();
                if (tempDiscardedCardname == "KING CRIMSON")
                {
                    discardedCard.sprite = standbycard_KingCrimson;
                    statistics.Mp_Current -= 12;
                    statistics.XP_Current += 1400;
                }
                else if (tempDiscardedCardname == "EUPHORIA")
                {
                    discardedCard.sprite = standbycard_Euphoria;
                    statistics.Mp_Current -= 15;
                    statistics.XP_Current += 2000;
                }
                else if (tempDiscardedCardname == "CUT")
                {
                    discardedCard.sprite = standbycard_Cut;
                    statistics.Mp_Current -= 3;
                    statistics.XP_Current += 500;
                }
                else
                {
                    discardedCard.sprite = null;
                }

                temp = discardedCard.color;
                temp.a = 0.5f;
                discardedCard.color = temp;

                cardslot1.Deselect();
            }
            else
            {

                if (cardslot1.GetCardName() == "KING CRIMSON")
                {
                    SelectedCard.sprite = standbycard_KingCrimson;
                }
                else if (cardslot1.GetCardName() == "EUPHORIA")
                {
                    SelectedCard.sprite = standbycard_Euphoria;
                }
                else if (cardslot1.GetCardName() == "CUT")
                {
                    SelectedCard.sprite = standbycard_Cut;
                }

                var temp = SelectedCard.color;
                if (temp.a < 1.0f)
                {
                    CancelInvoke();
                    InvokeRepeating("DoRotate", 0.0f, 3.5f);
                }
                temp.a = 1.0f;
                SelectedCard.color = temp;

                if (IsRotating)
                {
                    float prevRotation = SelectedCard.transform.rotation.z;
                    SelectedCard.transform.Rotate(new Vector3(0, 0, rotateMag));
                    //SelectedCard.transform.rotation.z (new Vector3(0, 0, SelectedCard.transform.eulerAngles.z + rotateMag);
                    if (SelectedCard.transform.rotation.z > 7 * Mathf.Deg2Rad)
                    {
                        rotateMag = -rotateMag;
                        SelectedCard.transform.Rotate(new Vector3(0, 0, rotateMag));
                    }
                    else if (SelectedCard.transform.rotation.z < -7 * Mathf.Deg2Rad)
                    {
                        rotateMag = -rotateMag;
                        SelectedCard.transform.Rotate(new Vector3(0, 0, rotateMag));
                    }
                    else if (SelectedCard.transform.rotation.z >= 0 && prevRotation < 0)
                    {
                        rotateMag = 0;
                        IsRotating = false;
                        SelectedCard.transform.eulerAngles = new Vector3(0, 0, 0);
                    }
                }

            }
        }
        else if (cardslot2.CheckIfSelected())
        {
            if (cardslot2.GetCardName() == "NONE") //Card was removed
            {
                rotateMag = 0;
                IsRotating = false;
                SelectedCard.transform.eulerAngles = new Vector3(0, 0, 0);

                var temp = SelectedCard.color;
                temp.a = 0.2f;
                SelectedCard.color = temp;

                SelectedCard.sprite = standbycard_none;

                string tempDiscardedCardname = cardslot2.GetDiscardedCard();
                if (tempDiscardedCardname == "KING CRIMSON")
                {
                    discardedCard.sprite = standbycard_KingCrimson;
                    statistics.Mp_Current -= 12;
                    statistics.XP_Current += 1400;
                }
                else if (tempDiscardedCardname == "EUPHORIA")
                {
                    discardedCard.sprite = standbycard_Euphoria;
                    statistics.Mp_Current -= 15;
                    statistics.XP_Current += 2000;
                }
                else if (tempDiscardedCardname == "CUT")
                {
                    discardedCard.sprite = standbycard_Cut;
                    statistics.Mp_Current -= 3;
                    statistics.XP_Current += 500;
                }
                else
                {
                    discardedCard.sprite = null;
                }

                temp = discardedCard.color;
                temp.a = 0.5f;
                discardedCard.color = temp;

                cardslot2.Deselect();
            }
            else
            {

                if (cardslot2.GetCardName() == "KING CRIMSON")
                {
                    SelectedCard.sprite = standbycard_KingCrimson;
                }
                else if (cardslot2.GetCardName() == "EUPHORIA")
                {
                    SelectedCard.sprite = standbycard_Euphoria;
                }
                else if (cardslot2.GetCardName() == "CUT")
                {
                    SelectedCard.sprite = standbycard_Cut;
                }

                var temp = SelectedCard.color;
                if (temp.a < 1.0f)
                {
                    CancelInvoke();
                    InvokeRepeating("DoRotate", 0.0f, 3.5f);
                }
                temp.a = 1.0f;
                SelectedCard.color = temp;

                if (IsRotating)
                {
                    float prevRotation = SelectedCard.transform.rotation.z;
                    SelectedCard.transform.Rotate(new Vector3(0, 0, rotateMag));
                    //SelectedCard.transform.rotation.z (new Vector3(0, 0, SelectedCard.transform.eulerAngles.z + rotateMag);
                    if (SelectedCard.transform.rotation.z > 7 * Mathf.Deg2Rad)
                    {
                        rotateMag = -rotateMag;
                        SelectedCard.transform.Rotate(new Vector3(0, 0, rotateMag));
                    }
                    else if (SelectedCard.transform.rotation.z < -7 * Mathf.Deg2Rad)
                    {
                        rotateMag = -rotateMag;
                        SelectedCard.transform.Rotate(new Vector3(0, 0, rotateMag));
                    }
                    else if (SelectedCard.transform.rotation.z >= 0 && prevRotation < 0)
                    {
                        rotateMag = 0;
                        IsRotating = false;
                        SelectedCard.transform.eulerAngles = new Vector3(0, 0, 0);
                    }
                }

            }
        }
        else if (cardslot3.CheckIfSelected())
        {
            if (cardslot3.GetCardName() == "NONE") //Card was removed
            {
                rotateMag = 0;
                IsRotating = false;
                SelectedCard.transform.eulerAngles = new Vector3(0, 0, 0);

                var temp = SelectedCard.color;
                temp.a = 0.2f;
                SelectedCard.color = temp;

                SelectedCard.sprite = standbycard_none;

                string tempDiscardedCardname = cardslot3.GetDiscardedCard();
                if (tempDiscardedCardname == "KING CRIMSON")
                {
                    discardedCard.sprite = standbycard_KingCrimson;
                    statistics.Mp_Current -= 12;
                    statistics.XP_Current += 1400;
                }
                else if (tempDiscardedCardname == "EUPHORIA")
                {
                    discardedCard.sprite = standbycard_Euphoria;
                    statistics.Mp_Current -= 15;
                    statistics.XP_Current += 2000;
                }
                else if (tempDiscardedCardname == "CUT")
                {
                    discardedCard.sprite = standbycard_Cut;
                    statistics.Mp_Current -= 3;
                    statistics.XP_Current += 500;
                }
                else
                {
                    discardedCard.sprite = null;
                }

                temp = discardedCard.color;
                temp.a = 0.5f;
                discardedCard.color = temp;

                cardslot3.Deselect();
            }
            else
            {

                if (cardslot3.GetCardName() == "KING CRIMSON")
                {
                    SelectedCard.sprite = standbycard_KingCrimson;
                }
                else if (cardslot3.GetCardName() == "EUPHORIA")
                {
                    SelectedCard.sprite = standbycard_Euphoria;
                }
                else if (cardslot3.GetCardName() == "CUT")
                {
                    SelectedCard.sprite = standbycard_Cut;
                }

                var temp = SelectedCard.color;
                if (temp.a < 1.0f)
                {
                    CancelInvoke();
                    InvokeRepeating("DoRotate", 0.0f, 3.5f);
                }
                temp.a = 1.0f;
                SelectedCard.color = temp;

                if (IsRotating)
                {
                    float prevRotation = SelectedCard.transform.rotation.z;
                    SelectedCard.transform.Rotate(new Vector3(0, 0, rotateMag));
                    //SelectedCard.transform.rotation.z (new Vector3(0, 0, SelectedCard.transform.eulerAngles.z + rotateMag);
                    if (SelectedCard.transform.rotation.z > 7 * Mathf.Deg2Rad)
                    {
                        rotateMag = -rotateMag;
                        SelectedCard.transform.Rotate(new Vector3(0, 0, rotateMag));
                    }
                    else if (SelectedCard.transform.rotation.z < -7 * Mathf.Deg2Rad)
                    {
                        rotateMag = -rotateMag;
                        SelectedCard.transform.Rotate(new Vector3(0, 0, rotateMag));
                    }
                    else if (SelectedCard.transform.rotation.z >= 0 && prevRotation < 0)
                    {
                        rotateMag = 0;
                        IsRotating = false;
                        SelectedCard.transform.eulerAngles = new Vector3(0, 0, 0);
                    }
                }

            }
        }
        else if (cardslot4.CheckIfSelected())
        {
            if (cardslot4.GetCardName() == "NONE") //Card was removed
            {
                rotateMag = 0;
                IsRotating = false;
                SelectedCard.transform.eulerAngles = new Vector3(0, 0, 0);

                var temp = SelectedCard.color;
                temp.a = 0.2f;
                SelectedCard.color = temp;

                SelectedCard.sprite = standbycard_none;

                string tempDiscardedCardname = cardslot4.GetDiscardedCard();
                if (tempDiscardedCardname == "KING CRIMSON")
                {
                    discardedCard.sprite = standbycard_KingCrimson;
                    statistics.Mp_Current -= 12;
                    statistics.XP_Current += 1400;
                }
                else if (tempDiscardedCardname == "EUPHORIA")
                {
                    discardedCard.sprite = standbycard_Euphoria;
                    statistics.Mp_Current -= 15;
                    statistics.XP_Current += 2000;
                }
                else if (tempDiscardedCardname == "CUT")
                {
                    discardedCard.sprite = standbycard_Cut;
                    statistics.Mp_Current -= 3;
                    statistics.XP_Current += 500;
                }
                else
                {
                    discardedCard.sprite = null;
                }

                temp = discardedCard.color;
                temp.a = 0.5f;
                discardedCard.color = temp;

                cardslot4.Deselect();
            }
            else
            {

                if (cardslot4.GetCardName() == "KING CRIMSON")
                {
                    SelectedCard.sprite = standbycard_KingCrimson;
                }
                else if (cardslot4.GetCardName() == "EUPHORIA")
                {
                    SelectedCard.sprite = standbycard_Euphoria;
                }
                else if (cardslot4.GetCardName() == "CUT")
                {
                    SelectedCard.sprite = standbycard_Cut;
                }

                var temp = SelectedCard.color;
                if (temp.a < 1.0f)
                {
                    CancelInvoke();
                    InvokeRepeating("DoRotate", 0.0f, 3.5f);
                }
                temp.a = 1.0f;
                SelectedCard.color = temp;

                if (IsRotating)
                {
                    float prevRotation = SelectedCard.transform.rotation.z;
                    SelectedCard.transform.Rotate(new Vector3(0, 0, rotateMag));
                    //SelectedCard.transform.rotation.z (new Vector3(0, 0, SelectedCard.transform.eulerAngles.z + rotateMag);
                    if (SelectedCard.transform.rotation.z > 7 * Mathf.Deg2Rad)
                    {
                        rotateMag = -rotateMag;
                        SelectedCard.transform.Rotate(new Vector3(0, 0, rotateMag));
                    }
                    else if (SelectedCard.transform.rotation.z < -7 * Mathf.Deg2Rad)
                    {
                        rotateMag = -rotateMag;
                        SelectedCard.transform.Rotate(new Vector3(0, 0, rotateMag));
                    }
                    else if (SelectedCard.transform.rotation.z >= 0 && prevRotation < 0)
                    {
                        rotateMag = 0;
                        IsRotating = false;
                        SelectedCard.transform.eulerAngles = new Vector3(0, 0, 0);
                    }
                }

            }
        }
        else
        {
            rotateMag = 0;
            IsRotating = false;
            SelectedCard.transform.eulerAngles = new Vector3(0, 0, 0);

            var temp = SelectedCard.color;
            temp.a = 0.2f;
            SelectedCard.color = temp;

            SelectedCard.sprite = standbycard_none;
        }


        if (CardsLeft > 0)
        {
            if (!cardslot1.Occupied())
            {
                CardsLeft -= 1;
                int RandomCard = Random.Range(0, 2 + 1);

                if (RandomCard == 0)
                {
                    cardslot1.SetCard("KING CRIMSON");
                }
                else if (RandomCard == 1)
                {
                    cardslot1.SetCard("EUPHORIA");
                }
                else if (RandomCard == 2)
                {
                    cardslot1.SetCard("CUT");
                }
            }
        }

        if (CardsLeft > 0)
        {
            if (!cardslot2.Occupied())
            {
                CardsLeft -= 1;
                int RandomCard = Random.Range(0, 2 + 1);

                if (RandomCard == 0)
                {
                    cardslot2.SetCard("KING CRIMSON");
                }
                else if (RandomCard == 1)
                {
                    cardslot2.SetCard("EUPHORIA");
                }
                else if (RandomCard == 2)
                {
                    cardslot2.SetCard("CUT");
                }
            }
        }

        if (CardsLeft > 0)
        {
            if (!cardslot3.Occupied())
            {
                CardsLeft -= 1;
                int RandomCard = Random.Range(0, 2 + 1);

                if (RandomCard == 0)
                {
                    cardslot3.SetCard("KING CRIMSON");
                }
                else if (RandomCard == 1)
                {
                    cardslot3.SetCard("EUPHORIA");
                }
                else if (RandomCard == 2)
                {
                    cardslot3.SetCard("CUT");
                }
            }
        }

        if (CardsLeft > 0)
        {
            if (!cardslot4.Occupied())
            {
                CardsLeft -= 1;
                int RandomCard = Random.Range(0, 2 + 1);

                if (RandomCard == 0)
                {
                    cardslot4.SetCard("KING CRIMSON");
                }
                else if (RandomCard == 1)
                {
                    cardslot4.SetCard("EUPHORIA");
                }
                else if (RandomCard == 2)
                {
                    cardslot4.SetCard("CUT");
                }
            }
        }

        cardslefttext.text = CardsLeft.ToString();
	}

    public void SetValidCardUse(bool _valid)
    {
        if (cardslot1.CheckIfSelected())
        {
            cardslot1.SetValidCardUse(_valid);
        }
        else if (cardslot2.CheckIfSelected())
        {
            cardslot2.SetValidCardUse(_valid);
        }
        else if (cardslot3.CheckIfSelected())
        {
            cardslot3.SetValidCardUse(_valid);
        }
        else if (cardslot4.CheckIfSelected())
        {
            cardslot4.SetValidCardUse(_valid);
        }
    }

    public string GetSelectedCardName()
    {
        if (cardslot1.CheckIfSelected())
        {
            return cardslot1.GetCardName();
        }
        else if (cardslot2.CheckIfSelected())
        {
            return cardslot2.GetCardName();
        }
        else if (cardslot3.CheckIfSelected())
        {
            return cardslot3.GetCardName();
        }
        else if (cardslot4.CheckIfSelected())
        {
            return cardslot4.GetCardName();
        }

        return "NONE";
    }

    public bool IsDragging()
    {
        return cardslot1.IsDragging() || cardslot2.IsDragging() || cardslot3.IsDragging() || cardslot4.IsDragging();
    }

}
