﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class inventory_Script : MonoBehaviour {

    public equipment_Stat helm;
    public equipment_Stat chest;
    public equipment_Stat pants;
    public equipment_Stat boots;

    public Image inventoryUItotal;

    public Image helmBox;
    public Image chestBox;
    public Image pantsBox;
    public Image bootsBox;

    public Text defense;

    void Placement(equipment_Stat _equipment, Image _owner)
    {
        if (Mathf.Sqrt(Mathf.Pow(Input.mousePosition.x - _owner.rectTransform.position.x, 2) + Mathf.Pow(Input.mousePosition.y - _owner.rectTransform.position.y, 2)) <= _owner.rectTransform.rect.width * _owner.rectTransform.lossyScale.x * 0.5)
        {
            if (_equipment.Equipped)
            {
                _equipment.beingDragged = false;
                _equipment.held = false;
            }
            else
            {
                _equipment.beingDragged = false;
                _equipment.held = false;
                _equipment.Equipped = true;
                _equipment.displayedPos = _owner.rectTransform.position;
            }

        }
        else
        {//send back to their respective locations, or choose a new location
            _equipment.beingDragged = false;
            _equipment.held = false;

            RectTransform temprct = inventoryUItotal.rectTransform;
            if (Input.mousePosition.x >= temprct.position.x - temprct.rect.width * _owner.rectTransform.lossyScale.x * 2
                && Input.mousePosition.x <= temprct.position.x + temprct.rect.width * _owner.rectTransform.lossyScale.x * 2
                && Input.mousePosition.y >= temprct.position.y - temprct.rect.height * _owner.rectTransform.lossyScale.y * 2
                && Input.mousePosition.y <= temprct.position.y + temprct.rect.height * _owner.rectTransform.lossyScale.y * 2)
            {
                Vector2 inventorytemp = new Vector2(0, 0);

                float intervalX = temprct.rect.width * _owner.rectTransform.lossyScale.x * 4 / (float)9;
                float intervalY = temprct.rect.height * _owner.rectTransform.lossyScale.y * 4 / (float)3;
                for (float x = 8; x >= 0; --x)
                {
                    if (Input.mousePosition.x >= temprct.position.x - temprct.rect.width * _owner.rectTransform.lossyScale.x * 2 + x * intervalX)
                    {
                        inventorytemp.x = x;
                        break;
                    }
                }
                for (float y = 2; y >= 0; --y)
                {
                    if (Input.mousePosition.y >= temprct.position.y - temprct.rect.height * _owner.rectTransform.lossyScale.y * 2 + y * intervalY)
                    {
                        inventorytemp.y = 2 - y;
                        break;
                    }
                }

                if (UnoccupiedSlot(inventorytemp.x, inventorytemp.y))
                {
                    _equipment.relativeEquipmentPos_Inventory = inventorytemp;
                    _equipment.Equipped = false;
                }

            };

        }
    }

    public void ReleaseDrag (equipment_Stat _equipment) {
        if (_equipment.beingDragged == false) return;

        string _ID = _equipment.ID;
        if (_ID == "Helmet")
        {
            Placement(_equipment, helmBox);

            //if (Mathf.Sqrt(Mathf.Pow(Input.mousePosition.x - helmBox.rectTransform.position.x, 2) + Mathf.Pow(Input.mousePosition.y - helmBox.rectTransform.position.y, 2)) <= helmBox.rectTransform.rect.width * helmBox.rectTransform.lossyScale.x * 0.5)
            //{
            //    if (_equipment.Equipped)
            //    {
            //        _equipment.beingDragged = false;
            //        _equipment.held = false;
            //    }
            //    else
            //    {
            //        _equipment.beingDragged = false;
            //        _equipment.held = false;
            //        _equipment.Equipped = true;
            //        _equipment.displayedPos = helmBox.rectTransform.position;
            //    }

            //}
            //else
            //{//send back to their respective locations, or choose a new location
            //    _equipment.beingDragged = false;
            //    _equipment.held = false;

            //    RectTransform temprct = inventoryUItotal.rectTransform;
            //    if (Input.mousePosition.x >= temprct.position.x - temprct.rect.width * helmBox.rectTransform.lossyScale.x * 2
            //        && Input.mousePosition.x <= temprct.position.x + temprct.rect.width * helmBox.rectTransform.lossyScale.x * 2
            //        && Input.mousePosition.y >= temprct.position.y - temprct.rect.height * helmBox.rectTransform.lossyScale.y * 2
            //        && Input.mousePosition.y <= temprct.position.y + temprct.rect.height * helmBox.rectTransform.lossyScale.y * 2)
            //    {
            //        Vector2 inventorytemp = new Vector2(0, 0);

            //        float intervalX = temprct.rect.width * helmBox.rectTransform.lossyScale.x * 4 / (float)9;
            //        float intervalY = temprct.rect.height * helmBox.rectTransform.lossyScale.y * 4 / (float)3;
            //        for (float x = 8; x >= 0; --x)
            //        {
            //            if (Input.mousePosition.x >= temprct.position.x - temprct.rect.width * helmBox.rectTransform.lossyScale.x * 2 + x * intervalX)
            //            {
            //                inventorytemp.x = x;
            //                break;
            //            }
            //        }
            //        for (float y = 2; y >= 0; --y)
            //        {
            //            if (Input.mousePosition.y >= temprct.position.y - temprct.rect.height * helmBox.rectTransform.lossyScale.y * 2 + y * intervalY)
            //            {
            //                inventorytemp.y = 2 - y;
            //                break;
            //            }
            //        }

            //        if (UnoccupiedSlot(inventorytemp.x, inventorytemp.y))
            //        {
            //            _equipment.relativeEquipmentPos_Inventory = inventorytemp;
            //            _equipment.Equipped = false;
            //        }

            //    };

            //}
        }
        else if (_ID == "Chest")
        {
            Placement(_equipment, chestBox);
        }
        else if (_ID == "Pants")
        {
            Placement(_equipment, pantsBox);
        }
        else if (_ID == "Boots")
        {
            Placement(_equipment, bootsBox);
        }
    }
	
    void Start()
    {

    }

	// Update is called once per frame
	void Update () {

        int DefenseValueTemp = 0;
        if (helm.Equipped)
        {
            DefenseValueTemp += 15;
        }
        if (chest.Equipped)
        {
            DefenseValueTemp += 27;
        }
        if (pants.Equipped)
        {
            DefenseValueTemp += 10;
        }
        if (boots.Equipped)
        {
            DefenseValueTemp += 13;
        }
        defense.text = "DEF: " + DefenseValueTemp.ToString();

    }

    public bool UnoccupiedSlot(float _x, float _y)
    {
        if (!helm.Equipped && helm.relativeEquipmentPos_Inventory.x == _x && helm.relativeEquipmentPos_Inventory.y == _y) return false;
        if (!chest.Equipped && chest.relativeEquipmentPos_Inventory.x == _x && chest.relativeEquipmentPos_Inventory.y == _y) return false;
        if (!pants.Equipped && pants.relativeEquipmentPos_Inventory.x == _x && pants.relativeEquipmentPos_Inventory.y == _y) return false;
        if (!boots.Equipped && boots.relativeEquipmentPos_Inventory.x == _x && boots.relativeEquipmentPos_Inventory.y == _y) return false;

        return true;
    }
}
