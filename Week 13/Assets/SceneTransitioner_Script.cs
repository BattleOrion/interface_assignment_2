﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneTransitioner_Script : MonoBehaviour {

    public float alpha = 1.0f;
    public Image Darkscreen;

    private string NextSceneEnter = "";
    private double CountdownToNextScene = 1.0;
    private bool EnteringNewScene = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (EnteringNewScene && CountdownToNextScene <= 0.0)
        {
            SceneManager.LoadScene(NextSceneEnter);
        }
        else if (EnteringNewScene)
        {
            alpha += Time.deltaTime;
            CountdownToNextScene -= Time.deltaTime;
            if (alpha > 1)
            {
                alpha = 1;
            }
        }
        else if (alpha > 0)
        {
            alpha -= Time.deltaTime;
            if (alpha < 0)
            {
                alpha = 0;
            }
        }
        var temp = Darkscreen.color;
        temp.a = alpha;
        Darkscreen.color = temp;

    }

    public void LoadScene(string nextScene)
    {
        if (alpha == 0)
        {
            NextSceneEnter = nextScene;
            EnteringNewScene = true;
            CountdownToNextScene = 1.0;
        }
    }

    public void LoadScene_Instant(string nextScene)
    {
        if (alpha == 0)
        {
            SceneManager.LoadScene(nextScene);
        }
    }

    public void Close()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            Application.Quit();
        }
    }

}
