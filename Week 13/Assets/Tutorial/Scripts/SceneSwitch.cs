﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitch : MonoBehaviour {
    
    [SerializeField]
    public string nextScene;

    // Use this for initialization
    void Start () { 

	}
	
    public void LoadScene()
    {
        SceneManager.LoadScene(nextScene);
    }
}
