﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JoyControl : MonoBehaviour {

    [SerializeField]
    private Image imgFG;

    [SerializeField]
    private Image imgBG;

    [SerializeField]
    private Canvas screenBG;

    float currentopacity = 0.2f;
  
	public void Dragging()
    {
        #if UNITY_EDITOR
            Vector3 newPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1);
        #elif UNITY_ANDROID
            Touch myTouch = Input.GetTouch(0);
            Vector3 newPos = new Vector3(myTouch.position.x, myTouch.position.y, 1);
        #else
            Vector3 newPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y,1);
        #endif
        imgFG.rectTransform.position = newPos;
        
        Vector2 centeredMousePos = new Vector2(Input.mousePosition.x * 2 - imgBG.rectTransform.position.x * 2, Input.mousePosition.y * 2 - imgBG.rectTransform.position.y * 2);
        centeredMousePos *= 0.2f;
        float angle = Mathf.Atan2(centeredMousePos.y, centeredMousePos.x)/* * Mathf.Rad2Deg*/;
        float length = Mathf.Sqrt(((centeredMousePos.x) * (centeredMousePos.x)) + ((centeredMousePos.y) * (centeredMousePos.y)));
        if (length > 50.0f)
        {
            length = 50.0f;
        }
        
        float newX = Mathf.Cos(angle) * length;
        float newY = Mathf.Sin(angle) * length;

        //var temp = imgBG.color;
        //temp.a += 0.05f;
        //if (temp.a > currentopacity) temp.a = currentopacity;
        //imgBG.color = temp;
        //temp = imgFG.color;
        //temp.a += 0.05f;
        //if (temp.a > currentopacity) temp.a = currentopacity;
        //imgFG.color = temp;

        //Debug.Log("a " + angle);
        //Debug.Log("b " + imgBG.rectTransform.anchoredPosition);

        //float newX = Mathf.Clamp(imgFG.rectTransform.anchoredPosition.x, -50, 50);
        //float newY = Mathf.Clamp(imgFG.rectTransform.anchoredPosition.y, -50, 50);

        //newX = Mathf.Clamp(newX, -Mathf.Abs(50 - newY), Mathf.Abs(50 - newY));
        //newY = Mathf.Clamp(newY, -Mathf.Abs(50 - newX), Mathf.Abs(50 - newX));

        imgFG.rectTransform.anchoredPosition = new Vector3(newX, newY, 1);
        
    }

    //public void OpacityUpdate()
    //{
    //    Debug.Log("AAAAAAAA");
    //    if (currentopacity > imgBG.color.a)
    //    {
    //        var temp = imgBG.color;
    //        temp.a += 0.05f;
    //        if (temp.a > currentopacity) temp.a = currentopacity;
    //        imgBG.color = temp;
    //        temp = imgFG.color;
    //        temp.a += 0.05f;
    //        if (temp.a > currentopacity) temp.a = currentopacity;
    //        imgFG.color = temp;
    //    }
    //    else if (currentopacity < imgBG.color.a)
    //    {
    //        var temp = imgBG.color;
    //        temp.a -= 0.05f;
    //        if (temp.a < currentopacity) temp.a = currentopacity;
    //        imgBG.color = temp;
    //        temp = imgFG.color;
    //        temp.a -= 0.05f;
    //        if (temp.a < currentopacity) temp.a = currentopacity;
    //        imgFG.color = temp;
    //    }
    //}

    public void ReturnOrigin()
    {
        imgFG.rectTransform.anchoredPosition = new Vector3(0, 0, 1);
        //imgBG.rectTransform.rotation = Quaternion.Euler(90, 0, 0);

        currentopacity = 0.2f;

        var temp = imgBG.color;
        temp.a = currentopacity;
        imgBG.color = temp;
        temp = imgFG.color;
        temp.a = currentopacity;
        imgFG.color = temp;
    }

    public void BecomeVisible()
    {
        //imgBG.rectTransform.rotation = Quaternion.Euler(0, 0, 0);

        currentopacity = 1.0f;

        var temp = imgBG.color;
        temp.a = currentopacity;
        imgBG.color = temp;
        temp = imgFG.color;
        temp.a = currentopacity;
        imgFG.color = temp;

        //var temp = imgBG.color;
        //temp.a += 0.05f;
        //if (temp.a > currentopacity) temp.a = currentopacity;
        //imgBG.color = temp;
        //temp = imgFG.color;
        //temp.a += 0.05f;
        //if (temp.a > currentopacity) temp.a = currentopacity;
        //imgFG.color = temp;
    }
}
