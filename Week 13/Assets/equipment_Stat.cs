﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class equipment_Stat : MonoBehaviour {

    public bool Equipped = false;

    public string ID = "";

    public Canvas canvas;

    public Image self;

    public Vector2 relativeEquipmentPos_Inventory = new Vector2(0, 0); //0-8, 0-2

    public Vector3 displayedPos;

    public bool beingDragged = false;
    public bool candrag = true;
    public bool held = false;

    public Sprite blockwhole;

    private float mgX = ((float)476 / (float)2560);
    private float mgY = ((float)298 / (float)1600);

    public double heldTime = 0.0;

	// Use this for initialization
	void Start () {
		
	}
	
    public void IncreaseHeldTime()
    {
        //heldTime += Time.deltaTime;
    }

    public void SetHold(bool _hold)
    {
        held = _hold;
    }

    public void SetDrag()
    {
        if (!candrag || heldTime >= 3.0)
            beingDragged = false;
        else
            beingDragged = true;
    }

    // Update is called once per frame
    void Update () {

        if (beingDragged)
        {
            held = false;
        }

        if (!held)
        {
            heldTime = 0.0;
        }
        else
        {
            heldTime += Time.deltaTime;
            if (heldTime > 3.0)
            {
                
                    var go = new GameObject("grid");
                    go.transform.SetParent(canvas.transform);
                    go.transform.SetAsLastSibling();
                    Image tempGridImg = go.AddComponent<Image>();
                    tempGridImg.sprite = blockwhole;
                    tempGridImg.raycastTarget = true;
                    go.transform.localScale = new Vector3(1, 1, 1);
                    RectTransform rt = go.GetComponent(typeof(RectTransform)) as RectTransform;
                    rt.sizeDelta = new Vector2(800.0f * 0.85f, 501.139f * 0.85f);
                    go.transform.position = canvas.transform.position;
                    //go.transform.localPosition = new Vector3(((float)x / (float)8) * canvas.pixelRect.width + (1 / 8) * canvas.pixelRect.width - 1 * canvas.pixelRect.width, ((float)y / (float)5) * canvas.pixelRect.height + (1 / 5) * canvas.pixelRect.height - 1 * canvas.pixelRect.height, 0);
                    Destroy(go, Time.deltaTime * 3.0f);



                var go2 = new GameObject("txt");
                go2.transform.SetParent(canvas.transform);
                go2.transform.SetAsLastSibling();
                Text ttxt = go2.AddComponent<Text>();
                switch(ID)
                {
                    case "Helmet":
                        ttxt.text = "Mythical helmet\nProvides high defense";
                        break;
                    case "Chest":
                        ttxt.text = "Armor of the Atomic Jaggerwuffer\nPulsates constantly";
                        break;
                    case "Pants":
                        ttxt.text = "A nicer pair of shorts";
                        break;
                    case "Boots":
                        ttxt.text = "Boots from Fallen Holographs";
                        break;
                    default:
                        ttxt.text = "N/A";
                        break;
                }
                ttxt.color = Color.black;
                ttxt.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
                ttxt.fontSize = 50;
                ttxt.alignment = TextAnchor.UpperCenter;
                ttxt.horizontalOverflow = HorizontalWrapMode.Overflow;
                ttxt.verticalOverflow = VerticalWrapMode.Overflow;
                go2.transform.position = canvas.transform.position;
                //RectTransform rt2 = go.GetComponent(typeof(RectTransform)) as RectTransform;
                //rt.sizeDelta = new Vector2(800, 501.139f);
                //go.transform.localPosition = new Vector3(((float)x / (float)8) * canvas.pixelRect.width + (1 / 8) * canvas.pixelRect.width - 1 * canvas.pixelRect.width, ((float)y / (float)5) * canvas.pixelRect.height + (1 / 5) * canvas.pixelRect.height - 1 * canvas.pixelRect.height, 0);
                Destroy(go2, Time.deltaTime * 3.0f);


            }
        }


        if (beingDragged)
        {
            self.rectTransform.sizeDelta = new Vector2(50, 100);
            self.rectTransform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y , 0);
        }
		else if (Equipped)
        {
            self.rectTransform.sizeDelta = new Vector2(16, 33);
            self.rectTransform.position = displayedPos;
        }
        else
        {
            self.rectTransform.sizeDelta = new Vector2(50, 100);
            self.rectTransform.localPosition = new Vector3(-84.19182f + 56.0f * relativeEquipmentPos_Inventory.x, 61.52369f - 101.6f * relativeEquipmentPos_Inventory.y, 0);
        }
	}
}
