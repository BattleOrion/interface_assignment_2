﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class SelectDataScript : MonoBehaviour {

    public string CardNameData;
    public int CardValueData;
    public Sprite CardSpriteData;

    public Image selectedCard;
    public Text selectedCardName;
    public Text selectedCardValue;


    // Use this for initialization
    void Start () {
        selectedCard.sprite = null;
        var temp = selectedCard.color;
        temp.a = 0.0f;
        selectedCard.color = temp;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetSelectedCardData()
    {
        selectedCard.sprite = CardSpriteData;

        var temp = selectedCard.color;
        temp.a = 1.0f;

        selectedCard.color = temp;

        selectedCardName.text = CardNameData;
        selectedCardValue.text = CardValueData.ToString();
    }
}
