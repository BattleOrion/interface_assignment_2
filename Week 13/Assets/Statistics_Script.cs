﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Statistics_Script : MonoBehaviour {

    public float Hp_Current = 507;
    public float Hp_Max = 507;
    public Text Hp_Text;
    public float Mp_Current = 50;
    public float Mp_Max = 100;
    public Text Mp_Text;
    public float XP_Current = 0;
    public float XP_CurrentDisplayed = 0;
    public float XP_Max = 6800;
    public float Level = 1;
    public Text Level_Text;
    public float XP_Percentage = 0;
    public Text XP_Percentage_Text;

    public Image HpBar_Visual;
    public Image MpBar_Visual;
    public Image XpBar_Visual;

    // Use this for initialization
    void Start () {
        Hp_Text.text = System.Math.Round(Hp_Current, 0).ToString() + "/" + System.Math.Round(Hp_Max, 0).ToString();
        Mp_Text.text = System.Math.Round(Mp_Current, 0).ToString() + "/" + System.Math.Round(Mp_Max, 0).ToString();
        Level_Text.text = "Lv." + System.Math.Round(Level, 0).ToString();
        XP_Percentage_Text.text = System.Math.Round(XP_Percentage, 2).ToString() + "%";
    }
	
	// Update is called once per frame
	void Update () {

        HpBar_Visual.transform.localScale = new Vector3(4.33f * (Hp_Current / Hp_Max), 0.2383652f, 0.2383652f);
        MpBar_Visual.transform.localScale = new Vector3(4.33f * (Mp_Current / Mp_Max), 0.2383652f, 0.2383652f);
        XpBar_Visual.transform.localScale = new Vector3(4.33f * (XP_CurrentDisplayed / XP_Max), 0.2383652f, 0.2383652f);

        if (Hp_Current > Hp_Max)
        {
            Hp_Current = Hp_Max;
        }

        Mp_Current += Time.deltaTime;
        if (Mp_Current > Mp_Max)
        {
            Mp_Current = Mp_Max;
        }

        if (XP_CurrentDisplayed < XP_Current)
        {
            float increaseamount = 1000 * Time.deltaTime * ((XP_Current + 1) / (XP_CurrentDisplayed + 1));
            if (increaseamount > 100) increaseamount = 100;

            XP_CurrentDisplayed += increaseamount;
            if (XP_Current < XP_CurrentDisplayed)
            {
                XP_CurrentDisplayed = XP_Current;
            }
        }

        while (XP_CurrentDisplayed >= XP_Max)
        {
            XP_Current -= XP_Max;
            XP_CurrentDisplayed -= XP_Max;
            Level += 1;
            XP_Max += 1000 * (Level * Level);
        }

        XP_Percentage = 100 * (XP_CurrentDisplayed / XP_Max);

        Hp_Text.text = System.Math.Round(Hp_Current, 0).ToString() + "/" + System.Math.Round(Hp_Max, 0).ToString();
        Mp_Text.text = System.Math.Round(Mp_Current, 0).ToString() + "/" + System.Math.Round(Mp_Max, 0).ToString();
        Level_Text.text = "Lv." + System.Math.Round(Level, 0).ToString();
        XP_Percentage_Text.text = System.Math.Round(XP_Percentage, 2).ToString() + "%";
    }
}
